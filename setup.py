from setuptools import find_packages, setup
setup(
    name='disturbtools',
    packages=find_packages(include=['disturbtools']),
    version='0.4.0',
    description='Useful functions for DISTURB',
    author='Cristina-Maria Cordun',
    license='None',
    install_requires=['astropy', 'numpy','h5py', 'datetime', 'healpy','scipy','pygdsm','matplotlib','numba'],
    tests_require=['pytest'],
    test_suite='tests',
    setup_requires=[
        'setuptools>=42',
        'wheel'
    ],
)
