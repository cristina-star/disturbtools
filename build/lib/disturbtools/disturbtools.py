from astropy import units as u
import numpy as np
import h5py
from os import listdir
from os.path import isfile,join
from datetime import datetime
import healpy as hp
from scipy import io, interpolate
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
from pygdsm import GSMObserver
from astropy import constants as const
from matplotlib import pyplot as plt
import numba as nb
import os

def subband_frequency(subband_number, clock_frequency=200*u.MHz):
    r'''
    Returns an astropy quantity containing the subband frequency.
    
    **Examples**
    >>> f = subband_frequency(128)
    >>> type(f)
    astropy.units.quantity.Quantity
    >>> print(f)
    25.0 MHz
    '''
    return (subband_number/512)*clock_frequency/2 




def loadXST(fn0):
    r'''
    Loads the correlation matrices for a given file name and path
    
    **Parameters**

    fn0: 'str'
        The path and the name of a specific h5 file
        
    **Returns**
     
       'list' - The correlation matrices for one observation
    '''
    D2=[]
    f = h5py.File(fn0[:], 'r')
    with h5py.File(fn0[:], 'r') as f:
        for s in f.keys():
            try:
                D=np.array(f[s]['values'])
                D2.append(D)
            except:
                continue
    D2=np.array(D2)
    return D2


def LoadFiles(fpath):
    r'''
    Returns the bands, correlation matrices and time of observation for all the observations in a folder.
    
    **Parameters**
    
    fpath: 'str'
        The path of the folder containing the raw h5 files
        
    **Returns** 
        'list': The band number of a specific observation ('bands')
        'list: The correlation matrices for each observation in the order of the 'bands' array 
        'list': The time of the start of the observation, in UTC, in the order of the 'bands' array.
    '''
    
    files = [f for f in listdir(fpath) if isfile(join(fpath,f))]
    Data=[]
    Bands=[]
    Time = []
    for f in files:
        ext=f.split('.')[-1]
        if ext!='h5': continue
        s=f.split('_')
        band=int(s[1][2:])
        ftime=s[2][:16]
        D=loadXST(join(fpath,f))
        D=np.array(D)
        #print("band=",band," time=",ftime," Data size=",D.shape)
        Bands.append(band)
        Data.append(D)
        Time.append(ftime)
    return Bands,Data,Time


def read_correlation_matrices(bands, data):
    r'''
    Calculated and returns the correlation matrices in an easy-to-use shape (no_of_bands, no_of_observations, no_of_antennas, no_of_antennas).
    
    **Parameters**
        
    bands: 'list'
        The list with the observed bands
        
    data: 'list'
        The correlation matrix of the correspondent band.
        
    **Returns**
        'ndarray' : The correlation matrix for all observations in the folder in a numpy array of shape
            (no_of_bands, no_of_observations, no_of_antennas, no_of_antennas)
    '''
    
    sis=[unb*12+si for unb in [8,10,11] for si in range(6)]
    print("Signal inputs:",sis)

    Nsis=len(sis)
    Nband=len(bands)
    CntL=np.zeros([512],dtype='int')
    bindex=sorted(range(Nband), key=lambda k: bands[k])
    bands2=[bands[x] for x in bindex]
    freqs=[bands2[x]/512*100 for x in range(Nband)]

    for cnt,x in enumerate(bindex):
        bnd=bands[x]
        Nb=data[x].shape[0]
        CntL[bnd]+=Nb
    Npnt=np.min(CntL)
    XSTL=np.zeros([512,np.max(CntL),Nsis,Nsis],dtype='complex')
    CntL=np.zeros([512],dtype='int')
    for cnt,x in enumerate(bindex):
        bnd=bands[x]
        D=data[x][:,sis,:] 
        D=D[:,:,sis]
        Nb=D.shape[0]
        XSTL[bnd,CntL[bnd]:CntL[bnd]+Nb]=D
        CntL[bnd]+=Nb

    XSTL=XSTL[:,:Npnt,:,:]
    print("bands,samples,InputxInput=",XSTL.shape)
    return XSTL


def covert_to_datetime(bands, times):
    r'''
    Coverts the time from a list of str, to a numpy array of datetime, in UTC
    
    **Parameters**

    bands: 'list'
        The list with the observed bands

    times: 'list'
        The starting observation time of the correspondent band.
        
    **Returns**
        'ndarray': The observation time for all bands in datetime   
         
    **Examples**
    >>> date_str = np.array(['2022-10-27-19-07'])
    >>> band = np.array([0])
    >>> date = covert_to_datetime(band, date_str)
    >>> type(date)
    numpy.ndarray
    >>> print(date)
    [datetime.datetime(2022, 10, 27, 19, 7)]
    '''
    date_time = []
    for b in np.array(bands):
        date_time.append(datetime.strptime(times[np.where(np.array(bands) == b)[0][0]], '%Y-%m-%d-%H-%M'))
            
    date_time = np.array(date_time)
    return date_time



def calculate_lm_coords(num_pix):
    r'''
    Calculates the l and m coordinate for a given number of pixels on the whole visible sky

    **Parameters**
    
    num_pix: 'int'
        The number of pixels that are convering the sky in one direction (vertically or horizonatlly) .The pixels are assumed to be sqare.

    **Returns**
        'ndarray': The m coordinate as a (num_pix,num_pix) array
        'ndarray': The l coordinate as a (num_pix,num_pix) array

    **Examples**
    >>> num_pix = 51
    >>> m,l = calculate_lm_coords(num_pix)
    >>> m.shape
    (51,51)
    >>> l.shape()
    (51,51)
    >>> np.nanmax(m)
    1
    >>> np.nanmin(m)
    -1
    '''
    m_1D = np.linspace(-1,1,num_pix)
    l_1D = np.linspace(1,-1,num_pix)
    m = np.zeros((num_pix,num_pix))
    l = np.zeros((num_pix,num_pix))

    for ind in range(num_pix):
        m[ind,:] = m_1D[ind]
        l[:,ind] = l_1D[ind]
        
    below_hor = np.where(m**2+l**2>1)
    l[below_hor] = np.nan
    m[below_hor] = np.nan

    return m, l


def ant_coords(dist):
    r'''
    Calculates the antenna positions for a given distance between antenna.
    DISTURB is assumed to be a 9x9 square array.
    The antennas are numbered from the most NW to the most SE one:
        N (x)
        |    0 1 2
        |    3 4 5
        |    6 7 8
        --------------- E (y)

    **Parameters**
    
    dist: 'float'
        The distance between two consecutive antennas in cm

    **Returns**
        'ndarray' of shape (9,2) array: the x ant y coordinate for each antenna

    **Examples**
    >>> dist = 300
    >>> coords = ant_coords(dist)
    >>> coords.shape
    (9,2)
    >>> coords
    [[-300  300]
    [   0.   300]
    [ 300  300]
    [-300    0. ]
    [   0.     0. ]
    [ 300    0. ]
    [-300 -300]
    [   0.  -300]
    [ 300 -300]] cm
    '''

    ant_pos = np.array([[-dist, dist], [0, dist], [dist, dist], [-dist, 0], [0,0], [dist, 0], [-dist,-dist], [0, -dist], [dist,-dist]])*u.cm
    return ant_pos


def compute_uv_coordinates(ant_pos):
    r'''
    Calculates the uv coordinates for a given vector with atenna positions

    **Parameters**
    
    ant_pos: 'ndarray' of float32
        The antenna coordinates in a numpy array of shape num_antennas x 2

    **Returns**
        'ndarray' of shape (num_ant, num_ant, 2): an array of all uv-coordinates with units of 'astropy.units.m'

    **Examples**
    >>> dist = 300
    >>> ant_pos = ant_coords(dist) # see ant_coords function
    >>> uv = compute_uv_coordinates(ant_pos)
    >>> uv.shape
    (9, 9, 2)
    >>> print(uv[0])
    [[ 0.     0.   ]
    [ 3.0     0.   ]
    [ 6.0     0.   ]
    [ 0.      -3.0 ]
    [ 3.0     -3.0 ]
    [ 6.0     -3.0 ]
    [ 0.      -6.0 ]
    [ 3.0     -6.0 ]
    [ 6.0     -6.0 ]] m
    '''
    num_ant = len(ant_pos)
    result = np.zeros((num_ant, num_ant, 2))*u.m
    for ant1 in range(num_ant):
        for ant2 in range(num_ant):
            result[ant1, ant2] = ant_pos[ant2] - ant_pos[ant1]
    return result


def convert_matlab_to_orthview(name, freq, num_pix, beam_format = 'DISTURB_array'):
    r'''
    Reads a MatLab file with the beam simulation and returns the array with all values.
    
    **Parameters**

    filename: 'str'
        The path and name of the MatLab file - !!! MUST HAVE A SPECIFIC STRUCTURE - see code !!!
    
    freq: 'ndarray' of float32
        The observing frequencies in units of astropy.units.MHz. 
        !!!! freq must be between the simulated frequencies of the beam !!!!
        
    num_pix: 'int'
        The number of pixels that are convering the sky in one direction (vertically or horizonatlly). The pixels are assumed to be sqare.

    beam_format: 'str' - 'DISTURB_array', 'LOFAR_array' or 'element'
        Specifies if the matlab file has the pattern of one element or patterns for all antennas
        
    **Returns**
        'ndarray' of float32 of shape (num_ant, polarization, component_index, freq, num_pix, num_pix).
        The patterns with component_index=1 are the ones for which the incident plane wave has polarization Etheta=1 V/m, Ephi=0. 
        The patterns with component_index=2 are the ones for which the incident plane wave has polarization Etheta=0, Ephi=1 V/m.
    '''
    from tqdm import tqdm
    
    if beam_format == 'DISTURB_array' or beam_format == 'element':
        file = io.loadmat(name)
        frequency_beam = np.array(file["Freq"])
        phi_beam = np.array(file["Phi"])
        theta_beam = np.array(file["Theta"])
        Vdiff_pol1_beam = np.array(file["Vdiff_pol1"]) #x
        Vdiff_pol2_beam = np.array(file["Vdiff_pol2"]) #y
    elif beam_format == 'LOFAR_array':
        files = []
        Vdiff_pol1_beam = []
        Vdiff_pol2_beam = []
        frequency_beam = []
        for f in os.listdir(name):
            if f.endswith('.mat'):
                files.append(f)
        files = np.sort(files)
        for f in files:
            print(f)
            file = io.loadmat(name+'/'+f)
            frequency_beam.append(file["Freq"])
            phi_beam = np.array(file["Phi"])
            theta_beam = np.array(file["Theta"])
            Vdiff_pol1_beam.append(file["Vdiff_pol1"]) #x
            Vdiff_pol2_beam.append(file["Vdiff_pol2"])#y
        Vdiff_pol1_beam = np.transpose(np.array(Vdiff_pol1_beam), (1,2,3,4,0))
        Vdiff_pol2_beam = np.transpose(np.array(Vdiff_pol2_beam), (1,2,3,4,0))
        frequency_beam = np.expand_dims(np.array(frequency_beam), axis = 0)[:,:,0,0]/10**6
    
    if beam_format == 'DISTURB_array':
        positions = np.array(file["Positions"])
        ind_antenna = np.array([6,7,8,3,4,5,0,1,2]) # because the numbering is different
    elif beam_format == 'LOFAR_array':
        positions = np.array(file["Positions"])
        ind_antenna = np.arange(96)
    elif beam_format == 'element':
        positions = np.array([0])
        ind_antenna = np.array([0])
    else:
        raise Exception("beam_format can only have the values of 'DISTURB_array', 'LOFAR_array' or 'element'")

    beam_pattern = np.ones((positions.shape[0],2,2,theta_beam.shape[1],phi_beam.shape[1],frequency_beam.shape[1]), dtype = np.complex128)

    for i in range(len(ind_antenna)):
        if beam_format == 'DISTURB_array':
            beam_pattern[i,0,0,...] = Vdiff_pol1_beam[ind_antenna[i],0,...]
            beam_pattern[i,0,1,...] = Vdiff_pol1_beam[ind_antenna[i],1,...]
            beam_pattern[i,1,0,...] = Vdiff_pol2_beam[ind_antenna[i],0,...]
            beam_pattern[i,1,1,...] = Vdiff_pol2_beam[ind_antenna[i],1,...]
        elif beam_format == 'LOFAR_array':
            beam_pattern[i,0,0,...] = Vdiff_pol1_beam[ind_antenna[i],0,...]
            beam_pattern[i,0,1,...] = Vdiff_pol1_beam[ind_antenna[i],1,...]
            beam_pattern[i,1,0,...] = Vdiff_pol2_beam[ind_antenna[i],0,...]
            beam_pattern[i,1,1,...] = Vdiff_pol2_beam[ind_antenna[i],1,...]
        elif beam_format == 'element':
            beam_pattern[i,0,0,...] = Vdiff_pol1_beam[0,...]
            beam_pattern[i,0,1,...] = Vdiff_pol1_beam[1,...]
            beam_pattern[i,1,0,...] = Vdiff_pol2_beam[0,...]
            beam_pattern[i,1,1,...] = Vdiff_pol2_beam[1,...]
        else:
            raise Exception("beam_format can only have the values of 'DISTURB_array', 'LOFAR_aray' or 'element'")
    beam_sky = None
    ov = GSMObserver()

    # values of lon and lat are not important
    ov.lon = '6.6031'
    ov.lat = '52.9145'
    ov.elev = 0
    ov.date = datetime(2022,10,4,12,0,0)
    ov.generate(freq[0].value) # frequency does not matter again, we only care about coordinates

    observing_location = EarthLocation(lat='52.9145', lon='6.6031', height=0*u.m)  
    date_time_str = datetime(2022,10,4,12,0,0).strftime("%Y-%m-%d %H:%M:%S")
    observing_time = Time(date_time_str) 
    altaz = AltAz(location=observing_location, obstime=observing_time)

    coord = SkyCoord(ov._observed_ra*u.deg, ov._observed_dec*u.deg) # default is ra dec
    alt_az_coords = coord.transform_to(altaz)

    az_deg = alt_az_coords.az.degree
    z_deg = 90-alt_az_coords.alt.degree
    
    z_2D = hp.orthview(z_deg, half_sky = True, xsize = num_pix, return_projected_map = True, fig = 7)
    az_2D = hp.orthview(az_deg, half_sky = True, xsize = num_pix, return_projected_map = True, fig = 7)

    beam_sky = np.zeros((positions.shape[0],2,2,frequency_beam.shape[1], num_pix, num_pix), dtype = np.complex128)

    print('Caculating ortographic projection...')
    
    for i in tqdm(range(frequency_beam.shape[1])):
        for ii in range(num_pix):
            for jj in range(num_pix):
                z = z_2D[ii,jj]
                az = az_2D[ii,jj]
                if z != -np.inf and az != -np.inf:
                    beam_sky[:,:,:,i,ii,jj] = beam_pattern[:,:,:,int(z), int(az/5), i]
                else:
                    beam_sky[:,:,:,i,ii,jj] = np.nan

    beam_real = np.zeros((positions.shape[0], 2,2,frequency_beam.shape[1], num_pix, num_pix))
    beam_imag = np.zeros((positions.shape[0], 2,2,frequency_beam.shape[1], num_pix, num_pix))

    beam_real = beam_sky.real
    beam_imag = beam_sky.imag

    beam_interp = np.zeros((positions.shape[0], 2,2, len(freq), num_pix, num_pix), dtype = np.complex128)

    print('Interpolating for missing frequencies...')
    for ant1 in tqdm(range(positions.shape[0])):
        for pol1 in range(2):
            for pol2 in range(2):
                for p1 in range(num_pix):
                    for p2 in range(num_pix):
                        f1 = interpolate.interp1d(frequency_beam[0], beam_real[ant1, pol1, pol2,:,p1,p2])
                        f2 = interpolate.interp1d(frequency_beam[0], beam_imag[ant1, pol1, pol2,:,p1,p2])
                        beam_interp[ant1, pol1, pol2,:,p1,p2] = f1(freq.value)+1j*f2(freq.value)
                    
    beam_real = None
    beam_imag = None

    return beam_interp

@nb.njit()
def _inner_loop(len_freq, num_ant, num_pix, beam_interp, integrate, b, dOm, beam_norm):
    for ind_f in range(len_freq):
        for ant in range(num_ant):
            for pix1 in range(num_pix):
                for pix2 in range(num_pix):
                    if np.isnan((beam_interp[ant,0,0,ind_f,pix1,pix2])):
                        continue
                    else:
                        integrate[ant,ind_f,:,:] += beam_interp[ant,:,:,ind_f,pix1,pix2]@beam_interp[ant,:,:,ind_f,pix1,pix2].T.conj()*dOm
            b[ant, ind_f] = (np.abs(np.linalg.det(integrate[ant,ind_f,:,:])))**(1/4)/(2*np.pi)**(1/2)


    for ant in range(num_ant):
        for ind_f in range(len_freq):
            beam_norm[ant,:,:,ind_f] = beam_interp[ant,:,:,ind_f]/b[ant,ind_f]


def normalize_beam(beam_interp):
    '''
    A function that normalized the input beam. The beam must have a shape of (num_ant, polarization, component_index, freq, num_pix, num_pix)
    This function has been optimized to run faster by David McKenna.
    '''
    len_freq = beam_interp.shape[3]
    num_ant = beam_interp.shape[0]
    pol = beam_interp.shape[1]
    num_pix = beam_interp.shape[4]
    ind = np.where(np.isnan(np.abs(beam_interp[0,0,0,0,:,:])))[0]
    dOm = 2*np.pi/(num_pix**2-ind.shape[0])
    integrate = np.zeros((beam_interp.shape[0],len_freq,pol,pol), dtype = np.complex64)

    beam_norm = np.copy(beam_interp)
    b = np.zeros((num_ant,len_freq))
    _inner_loop(len_freq, num_ant, num_pix, beam_interp, integrate, b, dOm, beam_norm)
    
    return beam_norm, b
    

@nb.njit
def calculate_matrices_product(sky_pow, beam, ant1, ant2, beam_format,sky_intensity_with_beam):
    '''
    Same paramteres as calculate_visibility but it makes the calculation 4 to 5 times faster with numba
    '''
    for pix1 in range(sky_pow.shape[2]):
        for pix2 in range(sky_pow.shape[3]):
            if beam_format == 'array':
                sky_intensity_with_beam[:,:,pix1,pix2] =  beam[ant1,:,:,pix1,pix2] @ sky_pow[:,:,pix1,pix2] @ beam[ant2,:,:,pix1,pix2].conj().T
            elif beam_format == 'element':
                sky_intensity_with_beam[:,:,pix1,pix2] =  beam[0,:,:,pix1,pix2] @ sky_pow[:,:,pix1,pix2] @ beam[0,:,:,pix1,pix2].conj().T
            else:
                raise Exception("beam_format must be 'array' or 'element'")
    return sky_intensity_with_beam

def calculate_visibility(sky_pow, freq, uv, l, m, use_beam, beam_format, beam, ant1, ant2):
    r'''
    Calculate the visibility for one baseline at a specific frequency,
        date and time for all polarizations. The function can use a beam model, if given one.
    
    **Paramters**

    sky_pow: 'ndarray' of float32 and shape (2, 2, num_pix, num_pix)
        The sky power model at a specific date, time and frequency for all polarizations
    
    freq: 'ndarray' of float32
        The observing frequencies in units of astropy.units.MHz
    
    uv: 'ndarray' of float32
        The x and y coordinates for a specific baseline in units of astropy.units.m
    
    l: 'ndarray' of float32 and shape (num_pix, num_pix)
        The l coordinate for the whole sky in orthview representation

    m: 'ndarray' of float32 and shape (num_pix, num_pix)
        The m coordinate for the whole sky in orthview representation

    use_beam: 'bool'
        False when the beam pattern is ignored, True otherwise
        
    beam_format: 'str' - 'array' or 'element'
        If use_beam is True, specifies if the matlab file has the pattern of one element or patterns for all antennas

    beam: 'ndarray' of complex128 and shape (num_ant, 2, 2, num_pix, num_pix)
        The beam in orthview representation for one frequency and all antennas and polarizations

    ant1: 'int'
        The index of the first antenna in the baseline

    ant2: 'int'
        The index of the second antenna in the baseline

    **Returns**

    'ndarray' of complex128 and shape (2,2,num_ant,num_ant)
        The Jones matrix of the simulated visibility for a specific basilne at a specific time and frequency in np.complex128.
    '''
    ind = np.where(np.isnan(np.abs(sky_pow[0,0,:,:])))[0]
    dOm = 2*np.pi/(sky_pow.shape[2]**2-ind.shape[0])
    if use_beam == True:
        sky_intensity_with_beam = np.empty_like(sky_pow)
        sky_intensity_with_beam = calculate_matrices_product(sky_pow, beam, ant1, ant2, beam_format,sky_intensity_with_beam)
    else:
        sky_intensity_with_beam = np.copy(sky_pow)

    # calculate visibility
    f = freq.to(u.Hz).value
    uv_l = uv[ant1, ant2].to(u.m).value
    uv_l = uv_l*f/const.c.value
    
    exp_power = -2*np.pi*1j*(uv_l[0]*l+uv_l[1]*m)
    visibilities = sky_intensity_with_beam*np.exp(exp_power)*dOm
    return np.nansum(visibilities, axis = (2,3), dtype = np.complex128)

def calculate_all_visibilities(latitude, longitude, elevation, date_time, freq,  uv, l, m, use_beam = False, beam_format = 'array', beam = None, path = None):
    r'''
    Calculate the visibilities for all baselines at a specific frequency,
        date, time and for both polarization. The function can use a beam model, if given one.
    The model can be saved at a specific path. If the function does not return anything, it makes it easier to run in parallel in python.

    **Parameters**

    latitude: 'str'
        The latitude of the observation location
    
    longitude: 'str'
        The longitude of the observation location
        
    elevation: 'float'
        The elevation of the observation location
    
    date_time: 'datetime'
        The date and time of the observation
    
    freq: 'ndarray' of float32
        The observing frequencies in units of astropy.units.MHz
    
    uv: 'ndarray' of float32
        The x and y coordinates for a specific baseline in units of astropy.units.m
    
    l: 'ndarray' of float32 and shape (num_pix, num_pix)
        The l coordinate for the whole sky in orthview representation

    m: 'ndarray' of float32 and shape (num_pix, num_pix)
        The m coordinate for the whole sky in orthview representation

    use_beam: 'bool'
        False when the beam pattern is ignored, True otherwise

    beam_format: 'str' - 'array' or 'element'
        If use_beam is True, specifies if the matlab file has the pattern of one element or patterns for all antennas

    beam: 'ndarray' of complex128 and shape (num_ant, 2, 2, num_pix, num_pix)
        If use_beam is True, the beam in orthview representation for one frequency and all antennas and polarizations
        
    path: 'str'
        Location to save the model (if not None)
        
    **Returns**

    'ndarray'of complex128 and shape (2,2,num_ant,num_ant)
        The simulated visibility for a all baselines at a specific time and frequency in np.complex128 in SI units.
    '''
    
    ov = GSMObserver()
    ov.lon = longitude
    ov.lat = latitude
    ov.elev = elevation
    ov.date = date_time
    ov.generate(freq.value)
    
    sky_temp = hp.orthview(ov.observed_sky, half_sky = True, title = 'T_sky', xsize = m.shape[0], return_projected_map = True, fig = 9)
    
    # R-J approximation
    sky_temp[sky_temp <= 0] = np.nan
    sky_intensity = (2*const.k_B.value*sky_temp/(const.c.value**2)*(freq.to(u.Hz).value)**2)
    sky_intensity_use = np.zeros((2,2, sky_intensity.shape[0], sky_intensity.shape[1]), dtype = np.complex128)
    sky_intensity_use[0,0] = sky_intensity/2
    sky_intensity_use[1,1] = sky_intensity/2
    
    vis_local = np.zeros((2,2,uv.shape[0],uv.shape[1]), dtype = np.complex128)
    for i in range(uv.shape[0]):
        for j in range(i,uv.shape[1]):
            vis_local[:,:,i,j] = calculate_visibility(sky_intensity_use, freq, uv, l, m, use_beam,beam_format,beam,i,j)
            vis_local[:,:,j,i] = np.conjugate(vis_local[:,:,i,j]) # IMPORTANT
    plt.close('all') # healpy orthview always opens a graph, and there is no setting to stop it
    if path == None:
        return vis_local
    else:
        np.save(path+str(freq), vis_local)   
        return


def scale_simulations_gain(vis, scale_beam_norm = None, coax_att = None, gain_RCU = None):
    r'''
    Scales the simulations to a number closed to the observed one.
    
    **Parameters**

    vis: 'ndarray of complex128 and shape (freq,num_ant,num_ant,2,2)
        Simulated correlation matrices 
        
    scale_beam_norm: 'ndarray' of float32 and shape(freq,num_ant)
        The scaling for the normalized beam

    coax_att: 'ndarray' of float32 and shape (freq,num_ant,2) 
        The attenuation of the cables in linear units of power

    gain_RCU: 'ndarray' of float32 and shape (freq,num_ant,2)  
        The gain of the receiver in linear units of power

    **Returns**

    'ndarray' of complex128 and shape as vis
        An array with the same shape as vis where the attenuation of the cables and RCU response have been applied to the Jones matrices
    '''


    len_freq = vis.shape[0]
    num_ant = vis.shape[1]
    pol = vis.shape[3]
    L_coax = np.zeros((len_freq,num_ant,pol,pol))
    g_RCU = np.zeros((len_freq,num_ant,pol,pol))
    beam_scale = np.zeros((len_freq,num_ant,pol,pol))
    
    if coax_att is None:
        L_coax[:,:,0,0] = 1
        L_coax[:,:,1,1] = 1
    else:
        L_coax[:,:,0,0] = coax_att[0]
        L_coax[:,:,1,1] = coax_att[1]
        
    if gain_RCU is None:
        g_RCU[:,:,0,0] = 1
        g_RCU[:,:,1,1] = 1
    else:
        g_RCU[:,:,0,0] = gain_RCU[0]
        g_RCU[:,:,1,1] = gain_RCU[1]

    if scale_beam_norm is None:
        beam_scale[:,:,0,0] = 1
        beam_scale[:,:,1,1] = 1
    else:
        beam_scale[:,:,0,0] = scale_beam_norm
        beam_scale[:,:,1,1] = scale_beam_norm
    
    vis_use = np.copy(vis)
    for f in range(len_freq):
        for ant1 in range(num_ant):
            for ant2 in range(num_ant):
                vis_use[f,ant1,ant2] = g_RCU[f,ant1] @ L_coax[f,ant1] @ beam_scale[f,ant1] @ vis[f,ant1,ant2] @ beam_scale[f,ant2].conj().T @ L_coax[f,ant2].conj().T @ g_RCU[f,ant2].conj().T
    return vis_use
    
    







def get_coherency(acm, ant1, ant2):
    return acm[2*ant1:2*ant1+2, 2*ant2:2*ant2+2]


def calculate_gains(obs, vis, alpha=0.5, epsilon=1e-5, refant=0, max_iter=200):
    r'''
    A function that calculate the gains for all observations at all frequencies and polarzations
    
    **Parameters**
    obs: 'ndarray' of complex128 and shape (freq,2*num_ant,2*num_ant)
        The observed visibities correlations matrices

    vis: 'ndarray' of complex128 and shape (freq,2*num_ant,2*num_ant)
        The modeled visibities correlations matrices
        !!! the vis must be in Jy !!!
        
    alpha : float in range 0.0--1.0
        Fraction of the difference new-old to update old
        with. Default: 0.5 for fastest convergence

    epsilon : float
        Precision at which to stop iterating. Default: 1e-6

    refant : int
        Antenna to use as phase reference.

    max_iter : int
        Number of iterations after which to stop iterating, even if
        convergence has not been reached.

    **Returns**
    'ndarray' of complex128 and shape (freq,num_ant,2,2)
        All the Jones matrices for gains for each frequency and antenna

    'ndarray' of 'bool' and shape(freq)
        True and False saying if the process finished for each frequency
    
    'ndarray' of 'int' and shape(freq)
        The number of iterations until convergence for each frequency
    '''
    from tqdm import tqdm
    num_ant = obs.shape[1]//2
    gains = np.zeros((obs.shape[0], num_ant,2,2),dtype = np.complex128)
    finished = np.zeros((obs.shape[0]))
    num_iter = np.zeros((obs.shape[0]))
    for ind_f in tqdm(range(obs.shape[0])):
        decomposition = [(get_coherency(obs[ind_f], a1,a2), a1, a2) for a1 in range(num_ant) for a2 in range(a1+1,num_ant) ]
        obs_vect = np.array([m for m,_,_ in decomposition], dtype=np.complex128)
        ant1 = np.array([a1 for _,a1,_ in decomposition], dtype=np.int64)
        ant2 = np.array([a2 for _,_,a2 in decomposition], dtype=np.int64)

        decomposition = [(get_coherency(vis[ind_f], a1,a2), a1, a2) for a1 in range(num_ant) for a2 in range(a1+1,num_ant) ]
        model_vect = np.array([m for m,_,_ in decomposition], dtype=np.complex128)
        gains[ind_f],_,num_iter[ind_f],finished[ind_f] = antsol_fulljones(obs_vect, model_vect, ant1, ant2,refant = refant, alpha= alpha, epsilon = epsilon, max_iter = max_iter)
    return gains, finished, num_iter
        


@nb.njit
def fast_det22(x):
    a = x[:,0,0]
    b = x[:,0,1]
    c = x[:,1,0]
    d = x[:,1,1]
    return a*d-b*c

@nb.njit
def fast_det22_single(x):
    a = x[0,0]
    b = x[0,1]
    c = x[1,0]
    d = x[1,1]
    return a*d-b*c

@nb.njit
def fast_inv22(x):
    a = x[:,0,0]
    b = x[:,0,1]
    c = x[:,1,0]
    d = x[:,1,1]
    y = np.zeros_like(x)
    invdet = 1/(a*d-b*c)
    y[:,0,0] = d*invdet
    y[:,0,1] = -b*invdet
    y[:,1,0] = -c*invdet
    y[:,1,1] = a*invdet
    return y


@nb.njit
def antsol_numerators_denominators(numerators, denominators, a1, a2, d, d_H, m, m_H, j_prev, j_prev_H, w):
    for i in range(len(a1)):
        numerators[a1[i]]   += w[i]*(d[i]   @ (j_prev[a2[i]]   @ m_H[i]))
        denominators[a1[i]] += w[i]*(m[i]   @ (j_prev_H[a2[i]] @ (j_prev[a2[i]] @ m_H[i])))
        numerators[a2[i]]   += w[i]*(d_H[i] @ (j_prev[a1[i]]   @ m[i]))
        denominators[a2[i]] += w[i]*(m_H[i] @ (j_prev_H[a1[i]] @ (j_prev[a1[i]] @ m[i])))
    pass
          

def antsol_fulljones(vis_data_vect, vis_model_vect,
                     antenna1_vect, antenna2_vect, jones_matrices = None,
                     antenna_flagged=None, # TODO
                     baseline_weights=None, alpha=0.5, epsilon=1e-6, refant=0,
                     max_iter=5000, debug=False):
    r'''Calculate Jones matrices that best fit the visibility data given
    the model visibilities. Function taken from lofartools library, written by Michiel Brentjens.

    **Parameters**

    vis_data_vect : np.array of complex128[N,2,2]
        This measured coherency matrices.

    vis_model_vect : np.array of complex128[N,2,2]
        The model coherency matrices.

    antenna1_vect : np.array of int[N]
        Antenna 1 of each visibility. Antennas are numbered 0...M-1
        consecutively, where M is the number of antennas.

    antenna2_vect : np.array of int[N]
        Antenna 2 of each visibility.  Antennas are numbered 0...M-1
        consecutively, where M is the number of antennas.

    jones_matrices : np.array of complex128[M,2,2] or None.  One
        initial Jones matrix per antenna. If None, make initial
        estimate to be equal to sqrt(det(data)/det(model))*I for each
        antenna. 

    antenna_flagged : np.array of bool[M] or None
        Antenna's whose entries are "True" will be excluded from the calculations
        and assigned the identity matrix as their jones matrix.
    
    baseline_weights : np.array of float32[N] or None
        Weight of each visibility. If None: absolute(det(vis_model_vect))
        to approximate inverse variance weighting if noise is uniform across
        baselines.

    alpha : float in range 0.0--1.0
        Fraction of the difference new-old to update old
        with. Default: 0.5 for fastest convergence

    epsilon : float
        Precision at which to stop iterating. Default: 1e-6

    refant : int
        Antenna to use as phase reference. HOW DOES THIS WORK WITH
        MATRICES? Default: 0

    max_iter : int
        Number of iterations after which to stop iterating, even if
        convergence has not been reached.

    debug : bool
        If True, antsol_fulljones also returns all intermediate results.

    **Returns**

    A tuple (solution_jones_matrices, antenna_flagged, num_iter, has_converged, intermediate_results)
    
    solution_jones_matrices is an np.array of complex128[M, 2, 2],
    where M is the number of antennas. antenna_flagged is an np.array
    of bool[M], where each entry that is True indicates that that
    antenna has failed to converge and its jones matrix is
    invalid. Jones matrices are all normalized such that the
    determinant of the Jones matrix of the reference antenna is
    real-valued. num_iter is the number of iterations it took to
    converge. has_converged is a boolean indicating that convergence
    was reached before max_iter was reached. intermediate_results
    contains intermediate results for each iteration. The later is
    only provided if "debug == True".
    
    
    **Examples**

    >>> full_pol_acm = np.load('model_correlation_matrix_50MHz_27.10.2022.npy')
    >>> num_ant = full_pol_acm.shape[0]//2
    >>> 
    >>> def get_coherency(acm, ant1, ant2):
    ...     return acm[2*ant1:2*ant1+2, 2*ant2:2*ant2+2]
    >>>
    >>> decomposition = [(get_coherency(full_pol_acm, a1,a2), a1, a2)
    ...                  for a1 in range(num_ant) for a2 in range(a1+1,num_ant) ]
    >>> coh_vect = np.array([m for m,_,_ in decomposition], dtype=np.complex128)
    >>> antenna_1 = np.array([a1 for _,a1,_ in decomposition], dtype=np.int64)
    >>> antenna_2 = np.array([a2 for _,_,a2 in decomposition], dtype=np.int64)
    >>>
    >>> data = coh_vect.copy()
    >>>
    >>> jones_3 = np.array([[3, -1j],[0.0, 2]], dtype=np.complex128)
    >>>
    >>> print(len(data))
    >>>     for i in range(len(data)):
    >>>         a1 = antenna_1[i]
    >>>         a2 = antenna_2[i]
    >>>
    >>>         if a1 == 3:
    >>>              data[i] = jones_3 @ data[i]
    >>>         if a2 == 3:
    >>>              data[i] = data[i] @ np.conj(jones_3).T
    >>>
    >>> sol,antenna_flagged,num_iter,has_converged,debug = imaging.antsol_fulljones(data*1e26, coh_vect*1e26,
    ...                                        antenna_1, antenna_2, max_iter=1000,debug=True,
    ...                                        baseline_weights=np.ones(len(data)))
    >>>

    '''
    num_vis = vis_data_vect.shape[0]
    num_ant = max(antenna1_vect.max(), antenna2_vect.max())+1
    vis_mask = fast_det22(vis_model_vect) == 0.0
    # 1: True, 0: False
    jones_mask = np.zeros(num_ant, dtype=np.int64)
    if antenna_flagged is not None:
        jones_mask[:] = antenna_flagged.astype(np.int64)[:]
    data_scale = np.mean(np.sqrt(fast_det22(vis_data_vect)))
    #print(data_scale)
    # For numerical stability, scale problem to order unity By scaling
    # both model and data by the same amount, we ensure that the jones
    # matrices themselves are not affected.
    data  = np.ascontiguousarray((vis_data_vect / data_scale).astype(np.complex128))
    model = np.ascontiguousarray((vis_model_vect / data_scale).astype(np.complex128))

    # Initialize jones_matrices
    jones = np.zeros((num_ant,2,2), dtype=np.complex128)
    if jones_matrices is None:
        count = 0
        scale = 0.0
        for j in range(num_vis):
            if not vis_mask[j]:
                thisscale = np.sqrt(np.sqrt(np.absolute(fast_det22_single(data[j]))/np.absolute(fast_det22_single(model[j]))))
                scale += thisscale
                count += 1
        if count > 0:
            scale /= count
        else:
            raise ValueError('No valid model matrices')
        for i in range(num_ant):
            jones[i][0,0] = scale
            jones[i][1,1] = scale
    else:
        jones[:] = jones_matrices[:]

    
    # Initialize the weights
    weights = np.ones(num_vis, dtype=np.float32)
    if baseline_weights is None:
        for j in range(num_vis):
            m = model[j]
            if vis_mask[j]:
                weights[j] = 0.0
            else:
                weights[j] = np.absolute(fast_det22_single(m))
    else:
        weights[:] = baseline_weights

    weights /= weights[weights != 0].mean()

    # TODO: check mask/flagging handling
    j_prev = np.ascontiguousarray(jones.copy().astype(np.complex128))
    num_iter = 0
    intermediate_results = []

    j_next = np.ascontiguousarray(np.zeros_like(j_prev))
    j_prev_H = np.ascontiguousarray(np.zeros_like(j_prev))
    numerators = np.ascontiguousarray(np.zeros_like(j_prev))
    denominators = np.ascontiguousarray(np.zeros_like(j_prev))
    data_H = np.ascontiguousarray(data.conj().transpose(0,2,1))
    model_H = np.ascontiguousarray(model.conj().transpose(0,2,1))
    den_det = np.ascontiguousarray(np.zeros(num_ant, dtype=np.complex128))

    good = (vis_mask == False)

    aca = np.ascontiguousarray
    a1 = aca(antenna1_vect[good])
    a2 = aca(antenna2_vect[good])
    d  = aca(data[good])
    d_H = aca(data_H[good])
    m = aca(model[good])
    m_H = aca(model_H[good])
    w = aca(weights[good])

    while True:
        j_next *= 0
        j_prev_H[:] = j_prev.conj().transpose(0,2,1)[:]
        numerators *= 0
        denominators *= 0

        # Neutralize visibilities having broken/non-fitting antennas
        w[jones_mask[a1]] = 0.0
        w[jones_mask[a2]] = 0.0

        antsol_numerators_denominators(numerators, denominators, a1, a2, d, d_H, m, m_H, j_prev, j_prev_H, w)
        
        den_det    = fast_det22(denominators)
        jones_mask[jones_mask==False] = den_det[jones_mask==False] == 0.0
        j_next[jones_mask == False] = numerators[jones_mask == False] @ fast_inv22(denominators[jones_mask == False])

        if debug: 
            intermediate_results.append({'j_prev': j_prev.copy(),
                                         'j_next_intermediate': j_next.copy(),
                                         'jones_mask': jones_mask.copy(),
                                         'weights': weights.copy(),
                                         'j_next': alpha*j_next + (1.0-alpha)*j_prev})

        j_next[:] = np.ascontiguousarray((alpha*j_next + (1.0-alpha)*j_prev).astype(np.complex128))

        # Normalize j_next by phase determinant refant
        unitary = np.array([[1.0, 0.0],[0.0, np.exp(-1.0j*np.angle(fast_det22_single(j_next[refant])))]], dtype=np.complex128)
        j_next = j_next @ unitary
        #for ant in range(num_ant):
        #    if jones_mask[ant]:
        #        j_next[ant,...] = np.eye(2,2, dtype=np.complex128)
        #    else:
        #        j_next[ant,...] = j_next[ant] @ unitary

        num_iter += 1
        diff = np.absolute(j_next-j_prev).ravel().max()/np.absolute(j_next).ravel().mean()

        if debug:
            intermediate_results[-1]['diff'] = diff
        if num_iter >= max_iter:
            break
        if  diff < epsilon:
            break
        else:
            j_prev[:] = j_next[:]
            
    for ant in range(num_ant):
        if jones_mask[ant]:
            j_next[ant,...] = np.eye(2,2, dtype=np.complex128)

    if debug:
        return (j_next, jones_mask, num_iter, num_iter < max_iter, intermediate_results)
    else:
        return (j_next, jones_mask, num_iter, num_iter < max_iter)

    
def find_jones_matrix_scaled(gains, beam_norm_factor = None, coax_att = None, gain_RCU = None):
    r'''
    Caclculates the complete Jones matrices with ANTSOL solutions and beam scaling (and cables and/or RCU if considered)  and normalizez them.

    **Parameters**

    gains: 'ndarray' of complex128 and shape (freq, num_ant, 2, 2)
        The complex gains resulted after ANTSOL function

    beam_norm_factor: 'ndarray' of float32 and shape (freq, num_ant)
        The scale of the beam after normalization 

    coax_att: 'ndarray' of float32 and shape (2,freq,num_ant) 
        The attenuation of the cables in linear units of power

    gain_RCU: 'ndarray' of float32 and shape (2,freq,num_ant)  
        The gain of the receiver in linear units of power

    **Returns** 

    'ndarray' of shape (freq, num_ant, 2, 2) and type complex128:  the normalized complex gains

    'ndarray' of shape (freq) and type float32: the general scaling factor for all antennas
    '''
    
    len_freq = gains.shape[0]
    num_ant = gains.shape[1]
    pol = gains.shape[2]
    complete_jones = np.empty_like(gains)
    L_coax = np.zeros((len_freq,num_ant,pol,pol))
    g_RCU = np.zeros((len_freq,num_ant,pol,pol))
    beam_scale = np.zeros((len_freq,num_ant,pol,pol))
    complete_jones_scale = np.copy(complete_jones)
    scale_factor_complete_jones = np.zeros((len_freq))
    
    if coax_att is None:
        L_coax[:,:,0,0] = 1
        L_coax[:,:,1,1] = 1
    else:
        L_coax[:,:,0,0] = coax_att[0]
        L_coax[:,:,1,1] = coax_att[1]
        
    if gain_RCU is None:
        g_RCU[:,:,0,0] = 1
        g_RCU[:,:,1,1] = 1
    else:
        g_RCU[:,:,0,0] = gain_RCU[0]
        g_RCU[:,:,1,1] = gain_RCU[1]

    if beam_norm_factor is None:
        beam_scale[:,:,0,0] = 1
        beam_scale[:,:,1,1] = 1
    else:
        beam_scale[:,:,0,0] = beam_norm_factor
        beam_scale[:,:,1,1] = beam_norm_factor
        
    complete_jones = gains @ g_RCU @ L_coax @ beam_scale

    
    for f in range(len_freq):
        sum_gains = np.sum(complete_jones[f], axis = 0)
        det_sqrt = np.sqrt(np.abs(np.linalg.det(sum_gains)))
        scale_factor_complete_jones[f] = det_sqrt/num_ant
        complete_jones_scale[f] = complete_jones[f]/scale_factor_complete_jones[f]
    return complete_jones_scale, scale_factor_complete_jones


def calculate_noises_Jy(obs, vis, gains, scale_factor_gains, antenna_avg = True, cross_noise = False):
    r'''
    Caclculates the sky noises in Jansky.

    **Parameters**

    obs: 'ndarray' of complex128 and shape (freq, num_ant, num_ant, 2, 2)
        The observed visibilities

    vis: 'ndarray' of complex128 and shape (freq, num_ant, num_ant, 2, 2)
        The visibilities for the simulated sky WITH NORMALIZED beam. !!! It is essential that the beam is normalized!!!

    gains: 'ndarray' of type complex128 and shape (freq, num_ant, 2, 2)   
        The normalized complex gains

    scale_factor_gains: 'ndarray' of type float32 and shape (freq) 
        The general scaling factor for all antennas

    antenna_avg: bool
        True if the noise from anttennas should be averaged. Only works if cross_noise = False
    
    cross_noise: bool
        True if the cross-correlation noises should also be returned. If True, antennna_avg is set to False.

    **Returns** 

    'ndarray' of type float32 and shape (freq, 2, 2): the noise matrix in Jansky averaged over all antennas (to better simulate the beamformer)
    '''

    if cross_noise == True:
        antenna_avg = False
    corrected = np.empty_like(obs)
    num_ant = obs.shape[1]
    len_freq = obs.shape[0]
    pol = obs.shape[3]
    for ind_f in range(len_freq):
        for ant1 in range(num_ant):
            for ant2 in range(num_ant):
                corrected[ind_f,ant1, ant2] = np.linalg.inv(gains[ind_f,ant1,:,:]) @ obs[ind_f,ant1, ant2,:,:] @ np.linalg.inv(gains[ind_f,ant2,:,:].conj().T)
                
        corrected[ind_f,:,:,:,:] = corrected[ind_f,:,:,:,:]/scale_factor_gains[ind_f]**2

    if cross_noise == False:
        noise = np.zeros((len_freq,num_ant,pol,pol))
        for ant in range(num_ant):
            noise[:,ant,:,:] = np.abs(corrected[:,ant,ant,:,:] - vis[:,ant,ant,:,:])
    else:
        noise = np.zeros((len_freq,num_ant,num_ant,pol,pol))
        for ant1 in range(num_ant):
            for ant2 in range(num_ant):
                noise[:,ant1,ant2,:,:] = np.abs(corrected[:,ant1,ant2,:,:] - vis[:,ant1,ant2,:,:])
    if antenna_avg == True:
        return np.nanmean(noise, axis = 1) # average for all antennas just like the beamformer
    else: 
        return noise

def apply_corrections(obs, gains, scale_factor_gains, noise = None):
    r'''
    Applies the corrections for an observations and returns the calibrated visibilities

    **Parameters**

    obs: 'ndarray' of complex128 and shape (freq, num_ant, num_ant, 2, 2)
        The observed visibilities

    gains: 'ndarray' of type complex128 and shape (freq, num_ant, 2, 2)   
        The normalized complex gains

    scale_factor_gains: 'ndarray' of type float32 and shape (freq) 
        The general scaling factor for all antennas

    noise: 'ndarray' of type float32 and shape (freq, 2, 2): 
        The noise matrix in Jansky averaged over all antennas
        
    **Returns** 

    'ndarray' of type complex128 and shape (freq, num_ant, num_ant, 2, 2): The calibrated visibilities
    '''
    corrected_vis = np.empty_like(obs)
    len_freq = obs.shape[0]
    num_ant = obs.shape[1]
    for ind_f in range(len_freq):
        for ant1 in range(num_ant):
            for ant2 in range(num_ant):
                corrected_vis[ind_f,ant1,ant2] = np.linalg.inv(gains[ind_f,ant1]) @ obs[ind_f,ant1,ant2] @  np.linalg.inv(gains[ind_f,ant2].conj().T)
        corrected_vis[ind_f,:,:,:,:] = corrected_vis[ind_f,:,:,:,:]/scale_factor_gains[ind_f]**2

    if noise is None:
        return corrected_vis
    else:
        for ant in range(num_ant):
            corrected_vis[:,ant,ant,:,:] = corrected_vis[:,ant,ant,:,:]-noise
        return corrected_vis

def pixel_brightness(acm_single_pol, l, m, uv, frequency, use_autocorr):
    r''''
    Calculates the brightness in a pixel of the image for a specific frequency and polarization

    **Parameters**

    acm_single_pol: 'ndarray' of complex128 and shape(num_ant, num_ant)
        The correlation matrix at a specific frequency and one polarization

    l: 'float'
        The l coordinate in units of astropy.units.rad or astropy.units.deg

    m: 'float'
        The m coordinate in units of astropy.units.rad or astropy.units.deg
    
        
    uv: 'ndarray' of float32
        The x and y coordinates for a specific baseline in units of astropy.units.m
    
    frequency: float32
        The observing frequency in units of astropy.units.MHz

    **Returns**

    float32: the brightness of a pixel for a specific frequency and polarization
    '''

    a = acm_single_pol.astype(np.complex128)
    n = a.shape[0]     # number of antennas
    f = frequency.to(u.Hz).value
    uv_l = uv.to(u.m).value
    uv_l = (uv_l*f/const.c.value)
    ll = l.to(u.rad).value
    mm = m.to(u.rad).value
    mask = np.ones((n, n), dtype=np.float32)
    if use_autocorr == False:
        mask -= np.diag(np.ones(n, dtype=np.float32)) # remove auto-correlations
    num_vis = mask.sum()

    brightness = (a*mask*np.exp(2j*np.pi*(uv_l[...,0]*ll + uv_l[...,1]*mm))).real.sum() #.real.sum for real values
    return brightness/num_vis   


def make_image(acm, num_pix_image, uv, frequency, l_range=(1.0, -1.0), m_range=(-1.0, 1.0), use_autocorr = False):
    r'''
    Makes an image from a correlation matrix. The returned image has its origin pixel (0,0) in bottom-left corner (south-east).

    **Parameters**

    acm: 'ndarray' of complex128 and shape(num_ant, num_ant)
        The correlation matrix at a specific frequency and one polarization

    num_pix_image: 'int'
        The number of pixels that the image should have in one direction. The pixels are rectangular.

    uv: 'ndarray' of float32
        The x and y coordinates for a specific baseline in units of astropy.units.m
    
    frequency: float32
        The observing frequency in units of astropy.units.MHz. 

    **Returns**

    'ndarray' of float32 and shape (num_pix,num_pix)
        The sky image as seen by the array
    'ndarray' of float 32 and shape (4)
        The extent of the image
    
    '''
    from tqdm.notebook import tqdm  
    img = np.zeros((num_pix_image, num_pix_image), dtype=np.float64)
    l_coor = np.linspace(*l_range, num_pix_image)*u.rad
    m_coor = np.linspace(*m_range, num_pix_image)*u.rad

    for m_ix, m in enumerate(m_coor): #tqdm(m_coor)
        for l_ix, l in enumerate(l_coor):
            img[m_ix, l_ix] = pixel_brightness(acm, l, m, uv, frequency, use_autocorr)
            if l.value**2+m.value**2>1:
                img[m_ix, l_ix] = np.nan
    pixel_sep = m_coor[1]-m_coor[0]
    half = pixel_sep.to(u.rad).value/2.0
    img_extent =(l_coor[0].to(u.rad).value+half, m_coor[0].to(u.rad).value-half, 
                 l_coor[-1].to(u.rad).value-half, m_coor[-1].to(u.rad).value+half) 
    return img, img_extent


