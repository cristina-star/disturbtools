from astropy import units as u
import numpy as np
import h5py
from os import listdir
from os.path import isfile,join
from datetime import datetime
import healpy as hp
from scipy import io, interpolate
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
from pygdsm import GSMObserver
from astropy import constants as const
from matplotlib import pyplot as plt
import multiprocessing
import time

def subband_frequency(subband_number, clock_frequency=200*u.MHz):
    r'''
    Returns an astropy quantity containing the subband frequency.
    
    **Examples**
    >>> f = subband_frequency(128)
    >>> type(f)
    astropy.units.quantity.Quantity
    >>> print(f)
    25.0 MHz
    '''
    return (subband_number/512)*clock_frequency/2 




def loadXST(fn0):
    r'''
    Loads the correlation matrices for a given file name and path
    
    Input: 'str'
        The path and the name of a specific h5 file
        
    Returns: 'list'
        The correlation matrices for one observation
    '''
    D2=[]
    f = h5py.File(fn0[:], 'r')
    with h5py.File(fn0[:], 'r') as f:
        for s in f.keys():
            try:
                D=np.array(f[s]['values'])
                D2.append(D)
            except:
                continue
    D2=np.array(D2)
    return D2


def LoadFiles(fpath):
    r'''
    Returns the bands, correlation matrices and time of observation for all the observations in a folder.
    
    Input: 'str'
        The path of the folder containing the raw h5 files
        
    Returns: 'list', 'list, 'list'
        - The band number of a specific observation ('bands')
        - The correlation matrices for each observation in the order of the 'bands' array 
        - The time of the start of the observation, in UTC, in the order of the 'bands' array.
    '''
    
    files = [f for f in listdir(fpath) if isfile(join(fpath,f))]
    Data=[]
    Bands=[]
    Time = []
    for f in files:
        ext=f.split('.')[-1]
        if ext!='h5': continue
        s=f.split('_')
        band=int(s[1][2:])
        ftime=s[2][:16]
        D=loadXST(join(fpath,f))
        D=np.array(D)
        #print("band=",band," time=",ftime," Data size=",D.shape)
        Bands.append(band)
        Data.append(D)
        Time.append(ftime)
    return Bands,Data,Time


def read_correlation_matrices(bands, data):
    r'''
    Calculated and returns the correlation matrices in an easy-to-use shape (no_of_bands, no_of_observations, no_of_antennas, no_of_antennas).
    
    Input: 'list', 'list'
        bands: the list with the observed bands
        data: the correlation matrix of the correspondent band.
        
    Returns: 'ndarray'
        The correlation matrix for all observations in the folder in a numpy array of shape
            (no_of_bands, no_of_observations, no_of_antennas, no_of_antennas)
    '''
    
    sis=[unb*12+si for unb in [8,10,11] for si in range(6)]
    print("Signal inputs:",sis)

    Nsis=len(sis)
    Nband=len(bands)
    CntL=np.zeros([512],dtype='int')
    bindex=sorted(range(Nband), key=lambda k: bands[k])
    bands2=[bands[x] for x in bindex]
    freqs=[bands2[x]/512*100 for x in range(Nband)]

    for cnt,x in enumerate(bindex):
        bnd=bands[x]
        Nb=data[x].shape[0]
        CntL[bnd]+=Nb
    Npnt=np.min(CntL)
    XSTL=np.zeros([512,np.max(CntL),Nsis,Nsis],dtype='complex')
    CntL=np.zeros([512],dtype='int')
    for cnt,x in enumerate(bindex):
        bnd=bands[x]
        D=data[x][:,sis,:] 
        D=D[:,:,sis]
        Nb=D.shape[0]
        XSTL[bnd,CntL[bnd]:CntL[bnd]+Nb]=D
        CntL[bnd]+=Nb

    XSTL=XSTL[:,:Npnt,:,:]
    print("bands,samples,InputxInput=",XSTL.shape)
    return XSTL


def covert_to_datetime(bands, times, manual_delay = False):
    r'''
    Coverts the time from a list of str, to a numpy array of datetime, in UTC
    
    Input: 'list', 'list'
        bands: the list with the observed bands
        times: the starting observation time of the correspondent band.
        
    Returns: 'ndarray'
        The observation time for all bands in datetime   
         
    **Examples**
    >>> date_str = np.array(['2022-10-27-19-07'])
    >>> band = np.array([0])
    >>> date = covert_to_datetime(band, date_str)
    >>> type(date)
    numpy.ndarray
    >>> print(date)
    [datetime.datetime(2022, 10, 27, 19, 7)]
    '''
    date_time = []
    for b in np.array(bands):
        date_time.append(datetime.strptime(times[np.where(np.array(bands) == b)[0][0]], '%Y-%m-%d-%H-%M'))
            
    date_time = np.array(date_time)
    return date_time



def calculate_lm_coords(num_pix):
    r'''
    Calculates the l and m coordinate for a given number of pixels on the whole visible sky

    Input: 'int'
        num_pix: the number of pixels that are convering the sky in one direction (vertically or horizonatlly) .The pixels are assumed to be sqare.

    Returns: 'ndarray', 'ndarray'
        - The m coordinate as a num_pix x num_pix array
        - The l coordinate as a num_pix x num_pix array

    **Examples**
    >>> num_pix = 51
    >>> m,l = calculate_lm_coords(num_pix)
    >>> m.shape
    (51,51)
    >>> l.shape()
    (51,51)
    >>> np.nanmax(m)
    1
    >>> np.nanmin(m)
    -1
    '''
    m_1D = np.linspace(-1,1,num_pix)
    l_1D = np.linspace(1,-1,num_pix)
    m = np.zeros((num_pix,num_pix))
    l = np.zeros((num_pix,num_pix))

    for ind in range(num_pix):
        m[ind,:] = m_1D[ind]
        l[:,ind] = l_1D[ind]
        
    below_hor = np.where(m**2+l**2>1)
    l[below_hor] = np.nan
    m[below_hor] = np.nan

    return m, l


def ant_coords(dist):
    r'''
    Calculates the antenna positions for a given distance between antenna.
    DISTURB is assumed to be a 9x9 square array.
    The antennas are numbered from the most NW to the most SE one:
        N (x)
        |    0 1 2
        |    3 4 5
        |    6 7 8
        --------------- E (y)

    Input: 'float'
        dist: the distance between two consecutive antennas in cm
    Return: 'ndarray'
        - 9 x 2 array: the x ant y coordinate for each antenna

    **Examples**
    >>> dist = 300
    >>> coords = ant_coords(dist)
    >>> coords.shape
    (9,2)
    >>> coords
    [[-300  300]
    [   0.   300]
    [ 300  300]
    [-300    0. ]
    [   0.     0. ]
    [ 300    0. ]
    [-300 -300]
    [   0.  -300]
    [ 300 -300]] cm
    '''

    ant_pos = np.array([[-dist, dist], [0, dist], [dist, dist], [-dist, 0], [0,0], [dist, 0], [-dist,-dist], [0, -dist], [dist,-dist]])*u.cm
    return ant_pos


def compute_uv_coordinates(ant_pos):
    r'''
    Calculates the uv coordinates for a given vector with atenna positions

    Input:
        ant_pos -  the antenna coordinates in a numpy array of shape num_antennas x 2
    Return:
        - an array of all uv-coordinates. The array has a shape of (num_ant, num_ant, 2), and should have unit of 'astropy.units.m'

    **Examples**
    >>> dist = 300
    >>> ant_pos = ant_coords(dist) # see ant_coords function
    >>> uv = compute_uv_coordinates(ant_pos)
    >>> uv.shape
    (9, 9, 2)
    >>> print(uv[0])
    [[ 0.     0.   ]
    [ 3.0     0.   ]
    [ 6.0     0.   ]
    [ 0.      -3.0 ]
    [ 3.0     -3.0 ]
    [ 6.0     -3.0 ]
    [ 0.      -6.0 ]
    [ 3.0     -6.0 ]
    [ 6.0     -6.0 ]] m
    '''
    num_ant = len(ant_pos)
    result = np.zeros((num_ant, num_ant, 2))*u.m
    for ant1 in range(num_ant):
        for ant2 in range(num_ant):
            result[ant1, ant2] = ant_pos[ant2] - ant_pos[ant1]
    return result


def convert_matlab_to_orthview(filename, freq, num_pix):
    r'''
    Reads a MatLab file with the beam simulation and returns the array with all values.
    This function takes about 30 seconds to run for num_pix = 51. 
    
    Input: 'str', 'ndarray', 'int'
        filename: The path and name of the MatLab file - !!! MUST HAVE A SPECIFIC STRUCTURE - see code !!!
        freq: The observing frequencies in units of astropy.units.MHz. 
        !!!! freq must be between the simulated frequencies of the beam !!!!
        num_pix: the number of pixels that are convering the sky in one direction (vertically or horizonatlly) .The pixels are assumed to be sqare.
        
    Returns: 'ndarray'
        - an array of shape num_ant x polarization x component_index x freq x num_pix x num_pix.
        The patterns with component_index=1 are the ones for which the incident plane wave has polarization Etheta=1 V/m, Ephi=0. 
        The patterns with component_index=2 are the ones for which the incident plane wave has polarization Etheta=0, Ephi=1 V/m.
    '''
    from tqdm import tqdm
    
    file = io.loadmat(filename)
    frequency_beam = np.array(file["Freq"])
    phi_beam = np.array(file["Phi"])
    theta_beam = np.array(file["Theta"])
    Vdiff_pol1_beam = np.array(file["Vdiff_pol1"]) #x
    Vdiff_pol2_beam = np.array(file["Vdiff_pol2"]) #y
    positions = np.array(file["Positions"])

    beam_pattern = np.ones((positions.shape[0],2,2,theta_beam.shape[1],phi_beam.shape[1],frequency_beam.shape[1]), dtype = np.complex128)
    ind_antenna = np.array([6,7,8,3,4,5,0,1,2]) # because the numbering is different
    for i in range(len(ind_antenna)):
        beam_pattern[i,0,0,...] = Vdiff_pol1_beam[ind_antenna[i],0,...]
        beam_pattern[i,0,1,...] = Vdiff_pol1_beam[ind_antenna[i],1,...]
        beam_pattern[i,1,0,...] = Vdiff_pol2_beam[ind_antenna[i],0,...]
        beam_pattern[i,1,1,...] = Vdiff_pol2_beam[ind_antenna[i],1,...]
    beam_sky = None
    ov = GSMObserver()

    # values of lon and lat are not important
    ov.lon = '6.6031'
    ov.lat = '52.9145'
    ov.elev = 0
    ov.date = datetime(2022,10,4,12,0,0)
    ov.generate(freq[0].value) # frequency does not matter again, we only care about coordinates

    observing_location = EarthLocation(lat='52.9145', lon='6.6031', height=0*u.m)  
    date_time_str = datetime(2022,10,4,12,0,0).strftime("%Y-%m-%d %H:%M:%S")
    observing_time = Time(date_time_str) 
    altaz = AltAz(location=observing_location, obstime=observing_time)

    coord = SkyCoord(ov._observed_ra*u.deg, ov._observed_dec*u.deg) # default is ra dec
    alt_az_coords = coord.transform_to(altaz)

    az_deg = alt_az_coords.az.degree
    z_deg = 90-alt_az_coords.alt.degree
    
    z_2D = hp.orthview(z_deg, half_sky = True, xsize = num_pix, return_projected_map = True, fig = 7)
    az_2D = hp.orthview(az_deg, half_sky = True, xsize = num_pix, return_projected_map = True, fig = 7)

    beam_sky = np.zeros((9,2,2,frequency_beam.shape[1], num_pix, num_pix), dtype = np.complex128)

    for i in tqdm(range(frequency_beam.shape[1])):
        for ii in range(num_pix):
            for jj in range(num_pix):
                z = z_2D[ii,jj]
                az = az_2D[ii,jj]
                if z != -np.inf and az != -np.inf:
                    beam_sky[:,:,:,i,ii,jj] = beam_pattern[:,:,:,int(z), int(az/5), i]
                else:
                    beam_sky[:,:,:,i,ii,jj] = np.nan

    beam_real = np.zeros((positions.shape[0], 2,2,frequency_beam.shape[1], num_pix, num_pix))
    beam_imag = np.zeros((positions.shape[0], 2,2,frequency_beam.shape[1], num_pix, num_pix))

    beam_real = beam_sky.real
    beam_imag = beam_sky.imag

    beam_interp = np.zeros((positions.shape[0], 2,2, len(freq), num_pix, num_pix), dtype = np.complex128)

    for ant1 in tqdm(range(positions.shape[0])):
        for pol1 in range(2):
            for pol2 in range(2):
                for p1 in range(num_pix):
                    for p2 in range(num_pix):
                        f1 = interpolate.interp1d(frequency_beam[0], beam_real[ant1, pol1, pol2,:,p1,p2])
                        f2 = interpolate.interp1d(frequency_beam[0], beam_imag[ant1, pol1, pol2,:,p1,p2])
                        beam_interp[ant1, pol1, pol2,:,p1,p2] = f1(freq.value)+1j*f2(freq.value)
                    
    beam_real = None
    beam_imag = None

    return beam_interp





def calculate_visibility(sky_pow, freq, uv, l, m, use_beam, beam, ant1, ant2):
    r'''
    Calculate the visibility for one baseline at a specific frequency,
        date and time for all polarizations. The function can use a beam model, if given one.
    
    Input: 'ndarray', 'ndarray', 'ndarray', 'ndarray', 'ndarray', 'bool', 'ndarray', 'int', 'int'
        sky_pow: 2 x 2 x num_pix x num_pix array - the sky power model at a specific date, time and frequency for all polarizations
        freq: The observing frequencies in units of astropy.units.MHz
        uv: the x and y coordinates for a specific baseline in units of astropy.units.m
        l: num_pix x num_pix - the l coordinate for the whole sky in orthview representation
        m: num_pix x num_pix - the m coordinate for the whole sky in orthview representation
        use_beam: False when the beam pattern is ignored, True otherwise
        beam: num_ant x 2 x 2 x num_pix x num_pix the beam in orthview representation for one frequency and all antennas and polarizations
        ant1: the index of the first antenna in the baseline
        ant2: the index of the second antenna in the baseline

    Returns: 'ndarray'
        The Jones matrix of the simulated visibility for a specific basilne at a specific time and frequency in np.complex128.
    '''
    
    if use_beam == True:
        sky_intensity_with_beam = np.empty_like(sky_pow)
        for pix1 in range(sky_pow.shape[2]):
            for pix2 in range(sky_pow.shape[3]):
                sky_intensity_with_beam[:,:,pix1,pix2] =  beam[ant1,:,:,pix1,pix2] @ sky_pow[:,:,pix1,pix2] @ beam[ant2,:,:,pix1,pix2].conj().T
    else:
        sky_intensity_with_beam = np.copy(sky_pow)


    # calculate visibility
    f = freq.to(u.Hz).value
    uv_l = uv[ant1, ant2].to(u.m).value
    uv_l = uv_l*f/const.c.value
    
    exp_power = -2*np.pi*1j*(uv_l[0]*l+uv_l[1]*m)
    visibilities = sky_intensity_with_beam*np.exp(exp_power)
    
    return np.nansum(visibilities, axis = (2,3))


def calculate_all_visibilities(latitude, longitude, elevation, date_time, freq,  uv, l, m, use_beam = False, beam = None, path = None):
    r'''
    Calculate the visibilities for all baselines at a specific frequency,
        date, time and for both polarization. The function can use a beam model, if given one.
    The model can be saved at a specific path. If the function does not return anything, it makes it easier to run in parallel in python.
    
    Input: 'string', 'string', 'float', 'datetime', 'float', 'ndarray', 'ndarray', 'ndarray', 'int', 'bool', 'ndarray', 'string'
        latitude: the latitude of the observation location
        longitude: the longitude of the observation location
        elevation: the elevation of the observation location
        date_time: the date and time of the observation
        freq: The observing frequencies in units of astropy.units.MHz
        uv: the x and y coordinates for a specific baseline in units of astropy.units.m
        l: num_pix x num_pix - the l coordinate for the whole sky in orthview representation
        m: num_pix x num_pix - the m coordinate for the whole sky in orthview representation
        use_beam: False when the beam pattern is ignored, True otherwise
        beam: num_ant x 2 x 2 x num_pix x num_pix the beam in orthview representation for one frequency and all antennas and polarizations
        path: location to save the model (if not None)
        
    Returns: 'ndarray'
        The simulated visibility for a specific basilne at a specific time and frequency in np.complex128.
    '''
    
    ov = GSMObserver()
    ov.lon = longitude
    ov.lat = latitude
    ov.elev = elevation
    ov.date = date_time
    ov.generate(freq.value)
    
    sky_temp = hp.orthview(ov.observed_sky, half_sky = True, title = 'T_sky', xsize = m.shape[0], return_projected_map = True, fig = 9)
    
    # R-J approximation
    sky_temp[sky_temp <= 0] = np.nan
    sky_intensity = (2*const.k_B.value*sky_temp/(const.c.value**2)*(freq.to(u.Hz).value)**2)
    sky_intensity_use = np.zeros((2,2, sky_intensity.shape[0], sky_intensity.shape[1]), dtype = np.complex128)
    sky_intensity_use[0,0] = sky_intensity/2
    sky_intensity_use[1,1] = sky_intensity/2
    
    vis_local = np.zeros((2,2,uv.shape[0],uv.shape[1]), dtype = np.complex128)
    for i in range(uv.shape[0]):
        for j in range(i,uv.shape[1]):
            vis_local[:,:,i,j] = calculate_visibility(sky_intensity_use, freq, uv, l, m, use_beam,beam,i,j)
            vis_local[:,:,j,i] = np.conjugate(vis_local[:,:,i,j]) # IMPORTANT
    plt.close('all') # healpy orthview always opens a graph, and there is no setting to stop it
    if path == None:
        return vis_local
    else:
        np.save(path+str(freq), vis_local)   
        return



    