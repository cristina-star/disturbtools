from disturbtools import disturbtools
from astropy import units as u
from datetime import datetime
import numpy as np

def test_subband_frequency():
    assert disturbtools.subband_frequency(256) == 50*u.MHz


def test_covert_to_datetime():
    assert disturbtools.covert_to_datetime(np.array([0]), np.array(['2022-10-27-19-07'])) == [datetime(2022, 10, 27, 19, 7)]

def test_calculate_lm_coords():
    assert np.nansum(disturbtools.calculate_lm_coords(50)[0]) < 10**(-10) and np.nansum(disturbtools.calculate_lm_coords(50)[1]) < 10**(-10)

def test_ant_coords():
    dist = 300
    coords = disturbtools.ant_coords(dist)
    assert np.sqrt((coords[0,0].value-coords[1,0].value)**2+(coords[0,1].value-coords[1,1].value)**2)-dist < 10**(-10)

def test_compute_uv_coordinates():
    uv = disturbtools.compute_uv_coordinates(disturbtools.ant_coords(300))
    assert np.sum(uv) == 0*u.m and uv[3,6,1] == -3.0*u.m and uv[3,6,0] == 0*u.m

def test_convert_matlab_to_orthview():
    freq = disturbtools.subband_frequency(np.arange(52,460))
    beam = disturbtools.convert_matlab_to_orthview('lba_3x3_array_pat.mat', freq, 51)
    print(beam[0,0,0,204,10:20,10:20])
    assert np.abs(np.abs(beam[0,0,0,204,20,30]) - 1.76) < 10**(-2) and np.abs(np.abs(beam[0,0,0,20,43,20]) - 0.10) < 10**(-2)

def test_calculate_gains():
    model = np.zeros((1,18,18), dtype = np.complex128)
    model[0] = np.load('model_correlation_matrix_50MHz_27.10.2022.npy')*10**26
    gains,_,_ = disturbtools.calculate_gains(model,model)
    assert np.abs(np.abs(np.linalg.det(gains[:,2,:,:]))-1)<10**(-8)

    