Before installing disturbtools, make sure that all the necesary libeary are installed by running the following command:

pip3 install numpy astropy h5py os datetime healpy scipy matplotlib numba

Then, you need to install the pygsm library using this link: https://github.com/telegraphic/pygdsm 

To install the library, run the following command:
    pip install git+https://gitlab.com/cristina-star/disturbtools.git

If that does not work, you can do it manually by following the steps below:

1. Download disturbtools library in your computer
2. Acess the dusturbtools folder
3. Run the following command:  
    pip3 install dist/disturbtools-0.4.0-py3-none-any.whl

!!! If the dist/disturbtools-0.4.0-py3-none-any.whl does not exist, you may need to build it first. In this case, use the following commands:
        python3 setup.py bdist_wheel
    followed by
        pip3 install dist/disturbtools-0.4.0-py3-none-any.whl
